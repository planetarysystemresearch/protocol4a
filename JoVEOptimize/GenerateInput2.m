function [] = GenerateInput2(qsca,wavelength,mreal,mimag,freeMean)



wavelength = num2str(wavelength);
output2 = num2str(2);

free = num2str(freeMean);
qsca = num2str(qsca);




if wavelength==1
    wavelength ='1.0';
elseif wavelength==2
    wavelength ='2.0';
elseif wavelength==3
    wavelength ='3.0';
else
    wavelength = num2str(wavelength);
end


file_name = 'input_2.in';



fin = fopen('input2.in','r');
fout = fopen(file_name,'w');

%input new values
while ~feof(fin)
   s = fgetl(fin);

   s = strrep(s, 'Wave', wavelength);
   s = strrep(s, 'difalb', qsca);
   s = strrep(s, 'freepa', free);

   s = strrep(s, 'Iden', output2);


   fprintf(fout,'%s \n',s);
end
fclose(fin);
fclose(fout);


