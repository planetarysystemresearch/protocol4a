function [qsca2] = runSIRIS(wavelength,mreal,mimag)


    % fix the env variable in order for the command line executable to work
    setenv('DYLD_LIBRARY_PATH', '/usr/local/bin/')


    %Generate input for run1

    GenerateInput1(wavelength,mreal,mimag)


    %first SIRIS4 run

    !./siris4 input_1.in pmatrix_1.in pmatrix_1.in


    delete('input_1.in')


    %format the pmatrix for input

    changeForm('outputS_001.out');

    %Get qsca

    qsca1 = getQsca('outputQ_001.out');

    %Get MeanFreePath
    
    free = getFree('outputQ_001.out');

    %Generate input for run2

    GenerateInput2(qsca1,wavelength,mreal,mimag,free)

    %second SIRIS4 run (with plane albedo)

    !./siris42 input_2.in pmatrix_2.in pmatrix_2.in

    %Get qsca

    qsca2 = getQsca('outputQ_002.out');
    
   
    

    delete('input_2.in', 'pmatrix_2.in', 'outputQ_001.out', 'outputS_001.out', 'outputQ_002.out', 'outputS_002.out')
