function [free] = getFree(fname)


%%%%%%%%%%%%%%%%%%%%%%%
% GET mean free-path length
%%%%%%%%%%%%%%%%%%%%%%%



fid = fopen(fname);
dataText = fgetl(fid);
while ~feof(fid)
    if strfind(dataText,'qext')
        ldata = textscan(dataText,'qext%s %f');
        qext = ldata{2};
        break
    end
    dataText = fgetl(fid);
end
fclose(fid);


fid2 = fopen(fname);
dataText = fgetl(fid2);
while ~feof(fid2)
    if strfind(dataText,'rhit')
        ldata = textscan(dataText,'rhit%s %f');
        a_perp = ldata{2};
        break
    end
    dataText = fgetl(fid2);
end
fclose(fid2);

a1 = 30.0;
a_perp = a_perp*a1;

v1 = 0.3;

avol = a1*exp(log(1.0+0.17^2));
kappa_ext = (3.0*v1*qext*a_perp^2)/(4*avol^3);
free = 1.0/kappa_ext;




