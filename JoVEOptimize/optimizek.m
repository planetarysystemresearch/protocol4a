% Computes refractive indices by using SIRIS4 and the bisection method.
% Author: Julia Martikainen


%upper bounds
filename1 = 'howardite-maximag.dat';
M = fopen(filename1);
maxpyr=textscan(M,'%s');

%lower bounds
filename2 = 'howardite-minimag.dat';
N = fopen(filename2);
minpyr=textscan(N,'%s');


%Values for spectrum
filename3 = 'HowarditePowder.dat';
L = fopen(filename3);
spectrum=textscan(L,'%s');

%Set the wavelength range
wavelength = [0.4:0.05:2.5];
% wavelength = [0.55,0.75,0.95,1.45,2.0];

y = [];

%save temp
fi2 = fopen('temp1.dat','w'); %Opens the file

mreal = 1.8;

t=1;

for i=1:length(wavelength)
    
    x = str2double(spectrum{1}{i});
    wave = wavelength(i)

    %Upper and lower bounds for k-values
    a = str2double(maxpyr{1}{i})
    b = str2double(minpyr{1}{i})

    mimag = (a+b)/2.0
    

    qsca = runSIRIS(wave,mreal,mimag)


    err=abs(qsca-x);


    while(err > 0.001)
        if(qsca > x)
            b = mimag;
        else
            a = mimag;
        end
        mimag = (a+b)/2.0
        t
        qsca = runSIRIS(wave,mreal,mimag)

        err = abs(qsca-x);
        
    end
    fprintf(fi2, '%f\n',  mimag);
    y = [y mimag];
    t=t+1
end

% write mimag to file
fclose(fi2);
fi = fopen('Derived_refractive_indices.dat','w'); %Opens the file
fprintf(fi, '%f\n',  y);
fclose(fi);




