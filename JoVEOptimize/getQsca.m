
function [qsca] = getQsca(fname)


%%%%%%%%%%%%%%%%%%%%%%%
% GET qsca
%%%%%%%%%%%%%%%%%%%%%%%



fid = fopen(fname);
dataText = fgetl(fid);
while ~feof(fid)
    if strfind(dataText,'qsca/qext')
        ldata = textscan(dataText,'qsca/qext%s %f');
        qsca = ldata{2};
        break
    end
    dataText = fgetl(fid);
end
fclose(fid);









