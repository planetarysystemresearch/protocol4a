function [] = GenerateInput1(wavelength,mreal,mimag)





mim = mimag;
mre = mreal;
mim = num2str(mim);
mre = num2str(mre);
wavelength = num2str(wavelength);
output1 = num2str(1);

if wavelength==1
    wavelength ='1.0';
elseif wavelength==2
    wavelength ='2.0';
elseif wavelength==3
    wavelength ='3.0';
else
    wavelength = num2str(wavelength);
end


file_name = 'input_1.in';


fin = fopen('input1.in','r');
fout = fopen(file_name,'w');

%input new values
while ~feof(fin)
   s = fgetl(fin);

   s = strrep(s, 'Wave', wavelength);
   s = strrep(s, 'repart', mre);
   s = strrep(s, 'impart', mim);
   s = strrep(s, 'Iden', output1);


   fprintf(fout,'%s \n',s);
end
fclose(fin);
fclose(fout);


