function ell = getMeanFreePath(Nwave, v, radiusList, qexts, a_perps, weights,filenameEll)
 
    Nradius = size(radiusList,2);
    aperps = cell(1,Nradius);
    qext = cell(1,Nradius);
   
    
    alku = 1;
    loppu = Nwave;
    a_vol = [];

    
    for i=1:Nradius
        aperps{1, i} = a_perps(alku:loppu) * radiusList(i);
         
        a_voltemp = radiusList(i) * exp(log(1.0 + 0.17^2)); %ensemble SIRIS4:lle
        a_vol = [a_vol a_voltemp];
        qext{1, i} = qexts(alku:loppu);
        
        alku = alku + Nwave;
        loppu = loppu + Nwave;
        
    end
    
    ell = [];
    for j=1:Nwave
        kappa_ext = 0;
        for i=1:Nradius   
            
            a_perpstemp = aperps{1, i};
            qexttemp = qext{1, i};
            

            kappa_exttemp = weights(i)*(3*v*qexttemp(j)*a_perpstemp(j)^2)/(4.0*a_vol(i)^3);
            kappa_ext = kappa_ext + kappa_exttemp;
            
        end
        
        elltemp = 1.0/kappa_ext;
        
        %mean free path averaged for different wavelengths
        ell = [ell elltemp];
    end

    
    
  
    fi1 = fopen(filenameEll,'w'); %Opens the file
    fprintf(fi1, '%f\n',  ell);
    fclose(fi1);
end

