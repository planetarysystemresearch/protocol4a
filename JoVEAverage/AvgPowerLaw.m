
%AVERAGE OVER POWERLAW (currently works only for computations carried out
%with SIRIS4)
%Author: Julia Martikainen

%SIRIS4 qsca,qext, and a_perp(rhit)

N = dlmread('Qscas.dat');
qscas = N(:,1);
qexts = N(:,2);
a_perps = N(:,3);


R = dlmread('Weights.dat');
weights= R(:,1);


%Output filenames
filenameAlb = 'avgAlbedoHowardite10to200Pl32.dat';
filenameEll = 'avgEllHowardite10to200Pl32.dat';

%volume density
v = 0.3;

%Wavelength range
waves = [0.4:0.05:2.5];
%waves = [0.55,0.75,0.95,1.45,2.0,2.5];
Nwave = size(waves,2)

%RADIUS 
radiusList = [5.0:5.0:100.0];

%get averaged sigmas for different wavelengths 
[sigma_stot,sigma_etot, sigma_sbefore] = getSigmas(Nwave, radiusList, qscas, qexts, a_perps,weights, filenameAlb);


%get averaged mean free paths for different wavelengths
ell = getMeanFreePath(Nwave, v, radiusList, qexts, a_perps, weights, filenameEll);



%create averaged Pmatrices

createPmatrices(Nwave, sigma_stot, sigma_sbefore, radiusList, weights);


