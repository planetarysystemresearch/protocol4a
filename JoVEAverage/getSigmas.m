function [sigma_stot,sigma_etot, sigma_sbefore] = getSigmas(Nwave,radiusList, qscas, qexts, a_perps, weights, filenameAlb)
 
    Nradius = size(radiusList,2);
    
    aperps = cell(1,Nradius);
    qsca = cell(1,Nradius);
    qext = cell(1,Nradius);
   
    
    alku = 1;
    loppu = Nwave;
    
    for i=1:Nradius
        aperps{1, i} =  a_perps(alku:loppu) * radiusList(i);
        qsca{1, i} = qscas(alku:loppu);
        qext{1, i} = qexts(alku:loppu);
        
        alku = alku + Nwave;
        loppu = loppu + Nwave;
        
    end
    

    sigma_sbefore = cell(1,Nradius);
    
    
     for j=1:Nradius
        sigma_sbeforetemp = [];
        
        aperpstemp = aperps{1, j};
        qscatemp = qsca{1, j};
        
        for i=1:Nwave  
                                
            temp = qscatemp(i) * pi *aperpstemp(i)^2;
            sigma_sbeforetemp = [sigma_sbeforetemp temp];           
            
        end
        
         %sigma_s before averaging for each size (wavelengths under sizes)
        sigma_sbefore{1, j} = sigma_sbeforetemp;
     end
    
  
     
     
    sigma_stot = [];
    sigma_etot = [];
     
    for j=1:Nwave
        sigma_s = 0;
        sigma_e = 0;
        for i=1:Nradius   
            
            aperpstemp = aperps{1, i};
            qscatemp = qsca{1, i};
            qexttemp = qext{1, i};
            
            
            sigma_stemp = qscatemp(j) * pi *aperpstemp(j)^2 *weights(i);
            sigma_s = sigma_s + sigma_stemp;
            sigma_etemp = qexttemp(j)* pi *aperpstemp(j)^2*weights(i);
            sigma_e = sigma_e + sigma_etemp;
                               
        end
        
        %sigmas averaged for different wavelengths
        sigma_stot = [sigma_stot sigma_s];
        sigma_etot = [sigma_etot sigma_e];
        
       
    end
    
    %calculate albedos and save them
    avgalb = [];
    for i = 1:Nwave     
        avgalb = [avgalb sigma_stot(i)/sigma_etot(i)];      
    end

    
    fi = fopen(filenameAlb,'w'); %Opens the file
    fprintf(fi, '%f\n',  avgalb);
    fclose(fi);
end

