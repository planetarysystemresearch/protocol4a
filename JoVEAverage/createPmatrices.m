function  createPmatrices(Nwave, sigma_stot, sigma_s, radiusList, weights)

    Nradius = size(radiusList,2);
    [a,b,c,d,e,f,g] = readFiles(Nwave, Nradius);
    
    a = [0.0:1.0:180.0];
    
    %convert to S-matrix
   
    sb = cell(Nwave,Nradius);
    sc = cell(Nwave,Nradius);
    sd = cell(Nwave,Nradius);
    se = cell(Nwave,Nradius);
    sf = cell(Nwave,Nradius);
    sg = cell(Nwave,Nradius);

    for i = 1:Nwave        
                avgbtemp = 0;
                avgctemp = 0;
                avgdtemp = 0;
                avgetemp = 0;
                avgftemp = 0;
                avggtemp = 0;
                
                for j = 1:Nradius

                    sigma_stemp = sigma_s{1, j};
                    btemp = b{j, i};
                    ctemp = c{j, i};
                    dtemp = d{j, i};
                    etemp = e{j, i};
                    ftemp = f{j, i};
                    gtemp = g{j, i};
                    
                    sbtemp = sigma_stemp(i)*btemp/(4.0*pi); % b 0-180 degrees
                    sctemp = sigma_stemp(i)*ctemp/(4.0*pi);
                    sdtemp = sigma_stemp(i)*dtemp/(4.0*pi);
                    setemp = sigma_stemp(i)*etemp/(4.0*pi);
                    sftemp = sigma_stemp(i)*ftemp/(4.0*pi);
                    sgtemp = sigma_stemp(i)*gtemp/(4.0*pi);
                    
                    sb{i, j} =  sbtemp;
                    sc{i, j} =  sctemp;
                    sd{i, j} =  sdtemp;
                    se{i, j} =  setemp;
                    sf{i, j} =  sftemp;
                    sg{i, j} =  sgtemp;
                
                end

    end
      
    
 % Average using weights
    for i = 1:Nwave
        avgb = [];
        avgc = [];
        avgd = [];
        avge = [];
        avgf = [];
        avgg = [];
            for n = 1:181
                avgbtemp = 0;
                avgctemp = 0;
                avgdtemp = 0;
                avgetemp = 0;
                avgftemp = 0;
                avggtemp = 0;
                
                %average over all sizes for each row
                for j = 1:Nradius

                    avgbtemp1 = sb{i, j};
                    avgctemp1 = sc{i, j};
                    avgdtemp1 = sd{i, j};
                    avgetemp1 = se{i, j};
                    avgftemp1 = sf{i, j};
                    avggtemp1 = sg{i, j};
                    
                    avgbtemp = avgbtemp + avgbtemp1(n)*weights(j);
                    avgctemp = avgctemp + avgctemp1(n)*weights(j);
                    avgdtemp = avgdtemp + avgdtemp1(n)*weights(j);
                    avgetemp = avgetemp + avgetemp1(n)*weights(j);
                    avgftemp = avgftemp + avgftemp1(n)*weights(j);
                    avggtemp = avggtemp + avggtemp1(n)*weights(j);
                end
             
                %make a list of all averaged rows
                avgb = [avgb avgbtemp];
                avgc = [avgc avgctemp];
                avgd = [avgd avgdtemp];
                avge = [avge avgetemp];
                avgf = [avgf avgftemp];
                avgg = [avgg avggtemp];
                
             
              
            end
            
           %convert back to P-matrix
           pavgb = avgb * 4.0*pi/(sigma_stot(i)); 
           pavgc = avgc * 4.0*pi/(sigma_stot(i)); 
           pavgd = avgd * 4.0*pi/(sigma_stot(i)); 
           pavge = avge * 4.0*pi/(sigma_stot(i)); 
           pavgf = avgf * 4.0*pi/(sigma_stot(i)); 
           pavgg = avgg * 4.0*pi/(sigma_stot(i)); 
           
           %save into p-matrix files
           foutname =['pmatrix_' sprintf('%d',i) '.in'];
           fid = fopen(foutname,'w'); %Opens the file
           fprintf(fid, '%f  %f  %f  %f  %f  %f  %f\n', [a; pavgb; pavgc; pavgd; pavge; pavgf; pavgg]);
           fclose(fid);
           
    end


            
%         
    end

