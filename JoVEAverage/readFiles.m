function [a, b, c, d, e, f, g] = readFiles(Nwave, Nradius)

    a = cell(Nradius,Nwave);
    b = cell(Nradius,Nwave);
    c = cell(Nradius,Nwave);
    d = cell(Nradius,Nwave);
    e = cell(Nradius,Nwave);
    f = cell(Nradius,Nwave);
    g = cell(Nradius,Nwave);
    
    for i=1:Nradius
        for j = 1:Nwave
            
            finname=['fold' sprintf('%d',i) '/pmatrix_' sprintf('%d',j) '.in'];


            [a1,b1,c1,d1,e1,f1,g1] = textread(finname, '%f %f %f %f %f %f %f');
            a1 = a1';
            b1 = b1';
            c1 = c1';
            d1 = d1';
            e1 = e1';
            f1 = f1';
            g1 = g1';
            
            a{i, j} = a1;
            b{i, j} = b1;
            c{i, j} = c1;
            d{i, j} = d1;
            e{i, j} = e1;
            f{i, j} = f1;
            g{i, j} = g1;
        end
    end
   
end

